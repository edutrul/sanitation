Para tener una mejor experiencia con la Aplicación Web

Por favor sigan las siguientes indicaciones:

Seguir este workflow:


========================
Username: mantenedor
Password: 123
Role: mantenedor
========================


========================
Username: ciudadano
Password: 123
Role: Usuario autenticado
========================



Crear Queja y verlo:
====================
0) Loguearse como Ciudadano.
1) Ir al bloque de "Empresas"
2) En la parte inferior del logo de Sedapal hay dos opciones "
Quejate!  Propon Mejora".
3) Hacer click en "Quejate!"
4) *Llenar los campos(título de la queja, descripción y adjuntar Foto.
5) Ver la queja que se creo con sus campos respectivos.
6) Ir al menu enlace "Quejas"
7) Y ver la queja que fue creada
8) Ir a la homePage y Ver también que la misma queja 
9) Si la queja contenia foto Entonces mirar en la sección derecha.
10) Ver que también aparece en el slideshow(efecto jquery) ya que fue
creado recientemente adjuntando una foto.

Nota:
* El campo "Company" Empresa está no disponible ya que al principio 
se le hizo click en Quejate. EL cual estaba vinculado a una empresa en específico.

Crear Propuesta de Mejora y verlo en homepage y Propuesta de Mejoras "Section":
===============================================================================
0) Loguearse como usuario ciudadano.
1) Ir al bloque de "Empresas"
2) En la parte inferior del logo de Sedapal hay dos opciones "
Quejate!  Propon Mejora".
3) Hacer click en "Propon Mejora!"
4) *Llenar los campos(título de la propuesta de mejora, descripción, adjuntar Video y adjuntar documento.
5) Para subir un video de youtube bastaria con hacer click en Select Media y click en la pestaña Web
y colocar la URL del video ejemplo: 
http://www.youtube.com/watch?v=jG9m9x0yQAs&feature=pyv&ad=5479913212&kw=alcantarillado
6) Ver la propuesta de mejora que se creo con sus campos respectivos.
7) Ir al menu enlace "Propuestas de mejoras"
8) Y ver que la propuesta de mejora fue creada
9) Ir a la homePage y Ver también que la misma propuesta de mejora está ahi.
10) Ver que la propuesta de mejora aparece en el bloque derecho "Propuestas de Mejora"


Comentar Comentar!!

1) Loguearse como ciudadano
2) Ir a cualquier contenido(haciendo click en su título) y tendra la oportunidad de comentar
3) Puede responder a un comentario y así formar un "hilo de comentarios"


Ver Reportes!!

1) Estar como usuario anónimo o ciudadano
2) ir al menu item "Reportes"
3) Buscar empresa por nombre o Encontrarlos ahi
4) Verificar el número de quejas y proppuestas de mejoras por cada empresa(Datos Reales)


Crear Empresa!

1) Estar logueado como usuario mantenedor
2) en lado derecho click en Add Content.
3) Click en Company
4) Llenar los datos de la empresa
5) Ver la Empresa(Company) creada
6) Fijarse que en el bloque derecha aparece la empresa Creada con su logo
(SI SOLO SI se le puso el logo) y adicionalmente 
dos acciones(Crear Queja!  Proponer Mejora!)


Ver Contenido

1) Estar como Usuario ańonimo y husmear por todos los bloques
2) ver los hilos de comentarios
3) Ver la homePage
4) Ver la lista de Empresas
5) Ver la lista de quejas
6) Ver la lista de Propuestas de Mejoras
7) Ver los Reportes)
8) Poder autenticarse(Crearse una nueva cuenta)


Crear una nueva cuenta:

1) Estar como usuario Anónimo
2) en el lado derecho hay un bloque User Login(Login de Usuario)
3) Click en "Create a new Account"
4) Llenar el Username
5) Llenar el correo
6) Click en Save
7) Te llegará un correo electrónico de bienvenida

Recuperar Contraseña

1) Estar como usuario Anónimo
2) En el lado derecho hay un bloque User Login(Login de Usuario)
3) Click en Request New Password(Recuperar nueva contraseña)
4) Te rediriga a un mini formulario)
5) Llenar el correo electrónico con el cual fue creado la cuenta.
6) Revisar tu correo electrónico con las indicaciones pertinentes para recuperar tu clave






